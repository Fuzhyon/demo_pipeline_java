/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epsi.services;

import fr.epsi.entities.BookTrain;
import fr.epsi.entities.Train;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 *
 * @author Chris
 */
public class GestionnaireService {
    
    
    // Attributs
    private static List<Train> listTrain = new ArrayList<>();
    private static List<BookTrain> listBT = new ArrayList<>();
    
    
    // Listes repr�sentants la BDD
    static{
        listTrain.add(new Train("TR123","Poitiers","Paris", 1250,200,50));
        listTrain.add(new Train("TR127","Poitiers","Paris", 1420,200,200));
        listTrain.add(new Train("TR129","Poitiers","Paris", 1710,50,0));
        listBT.add(new BookTrain(listTrain.get(0), "1", "1", 23, 15, 70));
        listBT.add(new BookTrain(listTrain.get(1), "2", "2", 38, 22, 35));
        listBT.add(new BookTrain(listTrain.get(2), "3", "1", 67, 124, 89));
    }
    
    // CRUD
    // TRAIN
    public static List<Train> readAllTrain(){
        return listTrain;
    }
    
    
    public static Train readTrainById(String id){
        for(Train t : listTrain){
            if(t.getNumTrain().equals(id)){
                return t;
            }
        }
        return null;
    }
    
    public static Train readTrainByArrivee(int arrivee) {
        for(Train t : listTrain){
            if(arrivee == t.getHeureArrivee()){
                return t;
            } 
        }
        return null;
    }

    public static Train readTrainByDepart(int depart) {
        for(Train t : listTrain){
            if(depart == t.getHeureDepart()){
                return t;
            } 
        }
        return null;
    }
    
    public static String postCreateTrain(Train nouvTrain) {
        boolean trainExiste = false;
        for(Train t : listTrain){
            if(nouvTrain.getNumTrain().equals(t.getNumTrain())){
                trainExiste = true;
            } 
        }
        if(trainExiste){
            return null;
        }else{
            listTrain.add(nouvTrain);
            return "Le train suivant a �t� cr�e : \n";
        }
    }

    public static String putUpdateTrain(Train majTrain) {
        for(Train trainInList : listTrain){
            if(trainInList.getNumTrain().equals(majTrain.getNumTrain())){
                trainInList.setHeureDepart(majTrain.getHeureDepart());
                trainInList.setHeureArrivee(majTrain.getHeureArrivee());
                trainInList.setVilleArrivee(majTrain.getVilleArrivee());
                trainInList.setVilleDepart(majTrain.getVilleDepart());
                trainInList.setPlacesDispo(majTrain.getPlacesDispo());
                return "Train mis � jour";
            }
        }
        return null;
    }

    public static String deleteTrainById(String id) {
        int position = -1;
        for(int i = 0; i<listTrain.size();i++){
            if(listTrain.get(i).getNumTrain().equals(id)){
                position = i;
            }
        }
        if(position != -1){
            String messageDelete = "Le train " + listTrain.get(position).getNumTrain() + " a �t� supprim�.";
            listTrain.remove(position);
            return messageDelete;
        }else 
        {
            return null;
        }
        
    }

    // BOOKTRAIN
    
    public static List<BookTrain> readAllReservation(){
        return listBT;
    }
    
    public static String postCreateReservation(BookTrain nouvelleReservation) {
        boolean reservationExiste = false;
        for(BookTrain bt : listBT){
            if(nouvelleReservation.getNumReservation().equals(bt.getNumReservation())){
                reservationExiste = true;
            } 
        }
        if(reservationExiste){
            return null;
        }else{
            for(Train t : listTrain){
                if(nouvelleReservation.getTrainReserve().getNumTrain().equals(t.getNumTrain())){
                    if(nouvelleReservation.getNombreDePlacesAReserver() > t.getPlacesDispo()){
                        return null;
                    }
                    else{
                        t.setPlacesDispo(t.getPlacesDispo() - nouvelleReservation.getNombreDePlacesAReserver());
                    }
                }   
            }
            listBT.add(nouvelleReservation);
            return "La r�servation suivante a �t� cr�e : \n";
        }
    }

    public static BookTrain readBookTrainById(String id) {
        for(BookTrain bt : listBT){
            if(bt.getNumReservation().equals(id)){
                return bt;
            }
        }
        return null;
    }

    public static String putUpdateReservation(BookTrain new_bt) {
        for(BookTrain bt : listBT){
            if(bt.getNumReservation().equals(new_bt.getNumReservation())){
                bt.setClasse(new_bt.getClasse());
                bt.setNumeroPlace(new_bt.getNumeroPlace());
                bt.setPrixReservation(new_bt.getPrixReservation());
                bt.setTrainReserve(new_bt.getTrainReserve());
                for (Train t : listTrain){
                    if(bt.getTrainReserve().getNumTrain() == t.getNumTrain()){
                        t.setPlacesDispo(t.getPlacesDispo() - new_bt.getNombreDePlacesAReserver());
                         bt.setTrainReserve(t);
                    }
                }
                return "R�servation mise � jour.";
            }
        }
        return null;
    }
  
    
    public static String removeBookTrain(String bookNumber) {
        BookTrain currentBookTrain = null;
        boolean trouve = false;
        for (BookTrain current : GestionnaireService.readAllReservation()) {
            if (current.getNumReservation().equals(bookNumber)) {
                currentBookTrain = current;
                trouve = true;
            }
        }
        if(trouve){
            GestionnaireService.readAllReservation().remove(currentBookTrain);
            return "Suppression de la r�servation � l'ID : "+ bookNumber;
        }
        else {
            return null;
        }
    }
    
    
    
    
}
