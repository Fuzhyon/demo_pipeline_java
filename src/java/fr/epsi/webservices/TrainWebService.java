/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epsi.webservices;

import fr.epsi.entities.Train;
import fr.epsi.services.GestionnaireService;
import java.lang.reflect.Field;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author Chris
 */
@Path("TrainWebService")
public class TrainWebService {

    // CRUD
    // CREATE 
    @POST
    @Path("createTrain")
    public Response postTrain(
            @QueryParam("numTrain") String numTrain,
            @QueryParam("villeDepart") String villeDepart,
            @QueryParam("villeArrivee") String villeArrivee,
            @QueryParam("heureDepart") int heureDepart,
            @QueryParam("heureArrivee") int heureArrivee,
            @QueryParam("placesDispo") int placesDispo) {
        Train nouvTrain = new Train(numTrain, villeDepart, villeArrivee, heureDepart, heureArrivee, placesDispo);
        String responseHTTP = GestionnaireService.postCreateTrain(nouvTrain);
        if (responseHTTP != null) {
            return Response.status(Response.Status.CREATED).entity(responseHTTP + nouvTrain).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).entity("Le train " + numTrain + " existe d�j�.").build();
        }
    }

    // READ
    @GET
    @Path("searchbyid={id}")
    public Response getTrainById(@PathParam("id") String id) {
        Train t = GestionnaireService.readTrainById(id);
        if (t == null) {
            return Response.status(Response.Status.NOT_FOUND).entity("Aucun train correspondant � l'ID n'a �t� trouv�.").build();
        }
        return Response.status(Response.Status.OK).entity(t.toString()).build();
    }

    @GET
    @Path("/all")
    public Response getAllTrains() {
        List<Train> trainList = GestionnaireService.readAllTrain();
        return Response.status(Response.Status.OK).entity(trainList.toString()).build();
    }

    @GET
    @Path("/searchbyarrivee={arrivee}")
    public Response getTrainByArrivee(@PathParam("arrivee") int arrivee) {
        Train t = GestionnaireService.readTrainByArrivee(arrivee);
        if (t == null) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity("Aucun train correspondant � l'heure d'arriv�e n'a �t� trouv�.").build();
        }
        return Response.status(Response.Status.OK).entity(t.toString()).build();
    }

    @GET
    @Path("/searchbydepart={depart}")
    public Response getTrainByDepart(@PathParam("depart") int depart) {
        Train t = GestionnaireService.readTrainByDepart(depart);
        if (t == null) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity("Aucun train correspondant � l'heure de d�part n'a �t� trouv�.").build();
        }
        return Response.status(Response.Status.OK).entity(t.toString()).build();
    }

    // UPDATE    
    @PUT
    @Path("updateTrain")
    public Response putUpdateTrain(
            @QueryParam("numTrain") String numTrain,
            @QueryParam("villeDepart") String villeDepart,
            @QueryParam("villeArrivee") String villeArrivee,
            @QueryParam("heureDepart") int heureDepart,
            @QueryParam("heureArrivee") int heureArrivee,
            @QueryParam("placesDispo") int placesDispo) {
        // Creation d'un train pour Update
        Train majTrain = new Train(numTrain, villeDepart, villeArrivee, heureDepart, heureArrivee, placesDispo);
        if (majTrain.verifyParams() == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity("L'un des param�tres fournis est null.").build();
        } else {
            String responseHTTP = GestionnaireService.putUpdateTrain(majTrain);
            if(responseHTTP == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("Le train n'a pu �tre mis � jour.").build();
            }
            else{
                return Response.status(Response.Status.OK).entity("Le train " + majTrain.getNumTrain() + " a �t� mis � jour.").build();
            }
        }
    }

    // DELETE
    @DELETE
    @Path("deleteTrain={id}")
    public Response deleteTrainById(@PathParam("id") String id) {
        String responseHTTP = GestionnaireService.deleteTrainById(id);
        if (responseHTTP == null) {
            return Response.status(Response.Status.NOT_FOUND).entity("Aucun train correspondant � l'heure de d�part n'a �t� trouv�.").build();
        }
        return Response.status(Response.Status.OK).entity(responseHTTP).build();
    }    
}
