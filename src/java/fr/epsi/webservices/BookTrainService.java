/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epsi.webservices;

import fr.epsi.entities.BookTrain;
import fr.epsi.entities.Train;
import fr.epsi.services.GestionnaireService;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author Chris
 */
@Path("reservation")
public class BookTrainService {

    // CRUD
    // CREATE 
    @POST
    @Path("createReservation")
    public Response postReservation(
            @QueryParam("numTrainReserve") String numTrainReserve,
            @QueryParam("numReservation") String numReservation,
            @QueryParam("classe") String classe,
            @QueryParam("nombreDePlacesAReserver") int nombreDePlacesAReserver,
            @QueryParam("numeroPlace") int numeroPlace,
            @QueryParam("prixReservation") int prixReservation) {
        Train t = GestionnaireService.readTrainById(numTrainReserve);
        if (t == null) {
            return Response.status(Response.Status.NOT_FOUND).entity("Aucun train correspondant � l'ID n'a �t� trouv�.").build();
        }
        BookTrain nouvelleReservation = new BookTrain(t, numReservation, classe, nombreDePlacesAReserver, numeroPlace, prixReservation);
        String responseHTTP = GestionnaireService.postCreateReservation(nouvelleReservation);
        if (responseHTTP != null) {
            return Response.status(Response.Status.CREATED).entity(responseHTTP + nouvelleReservation).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).entity("La r�servation " + numReservation + " n'est pas possible.").build();
        }
    }

    // READ
    @GET
    @Path("searchbyid={id}")
    public Response getReservationById(@PathParam("id") String id) {
        BookTrain bt = GestionnaireService.readBookTrainById(id);
        if (bt == null) {
            return Response.status(Response.Status.NOT_FOUND).entity("Aucune r�servation correspondante � l'ID n'a �t� trouv�.").build();
        }
        return Response.status(Response.Status.OK).entity(bt.toString()).build();
    }

    @GET
    @Path("/all")
    public Response getAllReservation() {
        List<BookTrain> btList = GestionnaireService.readAllReservation();
        return Response.status(Response.Status.OK).entity(btList.toString()).build();
    }


    // UPDATE    
    @PUT
    @Path("updateReservation")
    public Response putUpdateTrain(
            @QueryParam("numTrainReserve") String numTrainReserve,
            @QueryParam("numReservation") String numReservation,
            @QueryParam("classe") String classe,
            @QueryParam("nombreDePlacesAReserver") int nombreDePlacesAReserver,
            @QueryParam("numeroPlace") int numeroPlace,
            @QueryParam("prixReservation") int prixReservation) {
        // Creation d'un train pour Update
        Train t = GestionnaireService.readTrainById(numTrainReserve);
        if (t == null) {
            return Response.status(Response.Status.NOT_FOUND).entity("Aucun train correspondant � l'ID n'a �t� trouv�.").build();
        }
        
        BookTrain oldBookTrain = GestionnaireService.readBookTrainById(numReservation);
        if (oldBookTrain == null) {
            return Response.status(Response.Status.NOT_FOUND).entity("Aucune r�servation correspondante � l'ID n'a �t� trouv�e.").build();
        }
        Train oldTrain = oldBookTrain.getTrainReserve();
        oldTrain.setPlacesDispo(oldTrain.getPlacesDispo()+nombreDePlacesAReserver);
        
        BookTrain nouvelleReservation = new BookTrain(t, numReservation, classe, nombreDePlacesAReserver, numeroPlace, prixReservation);
        
        String responseHTTP = GestionnaireService.putUpdateReservation(nouvelleReservation);
        if(responseHTTP == null){
            return Response.status(Response.Status.BAD_REQUEST).entity("La r�servation n'a pu �tre mis � jour.").build();
        }
        else{
            return Response.status(Response.Status.OK).entity("La r�servation " + nouvelleReservation.getNumReservation() + " a �t� mise � jour.").build();
        }
        
    }

    // DELETE
    @DELETE
    @Path("deleteReservation={id}")
    public Response deleteTrainById(@PathParam("id") String id) {
        String responseHTTP = GestionnaireService.removeBookTrain(id);
        if (responseHTTP == null) {
            return Response.status(Response.Status.NOT_FOUND).entity("Aucune r�servation correspondant � l'id n'a �t� trouv�e.").build();
        }
        return Response.status(Response.Status.OK).entity(responseHTTP).build();
    }
}
