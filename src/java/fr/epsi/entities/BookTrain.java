/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epsi.entities;

/**
 *
 * @author Chris
 */
public class BookTrain {
    /* Attributs */
    private Train trainReserve;
    private String numReservation;
    private String classe;
    private int nombreDePlacesAReserver;
    private int numeroPlace;
    private int prixReservation;
    
    /* Constructeur */
    public BookTrain() {
    }
    
    public BookTrain(Train trainReserve, String numReservation, String classe, int nombreDePlacesAReserver, int numeroPlace, int prixReservation) {
        this.trainReserve = trainReserve;
        this.numReservation = numReservation;
        this.classe = classe;
        this.nombreDePlacesAReserver = nombreDePlacesAReserver; //Le nombre de places
        this.numeroPlace = numeroPlace;
        this.prixReservation = prixReservation;
    }
    
    /* M�thodes */

    @Override
    public String toString() {
        return "BookTrain{" + "trainReserve=" + trainReserve + ", numReservation=" + numReservation + ", classe=" + classe + ", nombreDePlacesAReserver=" + nombreDePlacesAReserver + ", numeroPlace=" + numeroPlace + ", prixReservation=" + prixReservation + '}';
    }
    
    
    /* Accesseurs Mutateurs */

    public Train getTrainReserve() {
        return this.trainReserve;
    }

    public void setTrainReserve(Train trainReserve) {
        this.trainReserve = trainReserve;
    }

    public String getNumReservation() {
        return numReservation;
    }

    public void setNumReservation(String numReservation) {
        this.numReservation = numReservation;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public int getNombreDePlacesAReserver() {
        return nombreDePlacesAReserver;
    }

    public void setNombreDePlacesAReserver(int nombreDePlacesAReserver) {
        this.nombreDePlacesAReserver = nombreDePlacesAReserver;
    }

    public int getNumeroPlace() {
        return numeroPlace;
    }

    public void setNumeroPlace(int numeroPlace) {
        this.numeroPlace = numeroPlace;
    }

    public int getPrixReservation() {
        return prixReservation;
    }

    public void setPrixReservation(int prixReservation) {
        this.prixReservation = prixReservation;
    }

    

    
}
