/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.epsi.entities;

/**
 *
 * @author Chris
 */
public class Train {
    
    /* Attributs */
    private String numTrain;
    private String villeDepart;
    private String villeArrivee;
    private int heureDepart;
    private int heureArrivee;
    private int placesDispo;
    
    
    /* Constructeurs */

    public Train() {
    }

    public Train(String numTrain, String villeDepart, String villeArrivee, int heureDepart, int heureArrivee, int placesDispo) {
        this.numTrain = numTrain;
        this.villeDepart = villeDepart;
        this.villeArrivee = villeArrivee;
        this.heureDepart = heureDepart;
        this.heureArrivee = heureArrivee;
        this.placesDispo = placesDispo;
    }

    
    /* M�thodes */
    public String verifyParams() {
        if(this.numTrain == null 
                || this.villeDepart == null 
                ||this.villeArrivee == null
                ||(this.heureDepart < 0 || this.heureDepart > 2359)
                ||(this.heureArrivee < 0 || this.heureArrivee > 2359)
                ||this.placesDispo <0 ){
            return null;
        }
        return "Param�tres valides.";
    }
    
    /* Accesseurs Mutateurs */
    public String getNumTrain() {
        return numTrain;
    }

    public void setNumTrain(String numTrain) {
        this.numTrain = numTrain;
    }

    public String getVilleDepart() {
        return villeDepart;
    }

    public void setVilleDepart(String villeDepart) {
        this.villeDepart = villeDepart;
    }

    public String getVilleArrivee() {
        return villeArrivee;
    }

    public void setVilleArrivee(String villeArrivee) {
        this.villeArrivee = villeArrivee;
    }

    public int getHeureDepart() {
        return heureDepart;
    }

    public void setHeureDepart(int heureDepart) {
        this.heureDepart = heureDepart;
    }

    public int getHeureArrivee() {
        return heureArrivee;
    }

    public void setHeureArrivee(int heureArrivee) {
        this.heureArrivee = heureArrivee;
    }

    public int getPlacesDispo() {
        return placesDispo;
    }

    public void setPlacesDispo(int placesDispo) {
        this.placesDispo = placesDispo;
    }

    @Override
    public String toString() {
        return "Train{" + "numTrain=" + numTrain + ", villeDepart=" + villeDepart + ", villeArrivee=" + villeArrivee + ", heureDepart=" + heureDepart + ", heureArrivee=" + heureArrivee + ", placesDispo=" + placesDispo + '}';
    }

    

    
    
    
}
